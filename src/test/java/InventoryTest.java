import com.furniturewarehouse.dao.Impl.ProductDao;
import com.furniturewarehouse.dao.ProductDaoInterface;
import com.furniturewarehouse.entity.Product;
import com.furniturewarehouse.service.Impl.Inventory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InventoryTest {
    @Mock
    private ProductDao mockProductDao;

    private Inventory inventory;

    @Before
    public void setUp() {
        // Initialize the mockProductDao
        mockProductDao = mock(ProductDao.class);

        // Create the inventory instance with the mockProductDao
        inventory = new Inventory(mockProductDao);
    }

    @Test
    public void testSearchProducts() {
        // Create a sample list of products
        List<Product> searchResults = new ArrayList<>();
        searchResults.add(new Product(1, "Chair", "Furniture", 99.99, 10));
        searchResults.add(new Product(2, "Table", "Furniture", 199.99, 5));

        // Set up the behavior of the productDao.searchProducts method
        when(mockProductDao.searchProducts("Chair", 0, 100))
                .thenReturn(searchResults);

        // Perform searchProducts operation
        List<Product> results = inventory.searchProducts("Chair", 0, 100);

        // Verify that the productDao.searchProducts method was called with the search parameters
        verify(mockProductDao).searchProducts("Chair", 0, 100);

        // Verify the results
        assertEquals(searchResults, results);
    }



}
