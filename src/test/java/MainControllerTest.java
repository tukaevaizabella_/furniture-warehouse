import com.furniturewarehouse.controller.MainController;
import com.furniturewarehouse.service.Impl.Inventory;
import com.furniturewarehouse.entity.Product;
import com.furniturewarehouse.service.InventoryInterface;
import com.furniturewarehouse.view.CommandPrompt;
import com.furniturewarehouse.view.ProductDetails;
import com.furniturewarehouse.view.SearchResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class MainControllerTest {
    @Mock
    private InventoryInterface inventory;
    @Mock
    private CommandPrompt commandPrompt;
    @Mock
    private ProductDetails productDetailsView;
    @Mock
    private SearchResults searchResultsView;

    private MainController mainController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mainController = new MainController(inventory, commandPrompt, searchResultsView, productDetailsView);
    }



    @Test
    void testHandleAddProductCommand() {
        when(commandPrompt.getInput(anyString())).thenReturn("Product Name");
        when(commandPrompt.getDoubleInput(anyString())).thenReturn(9.99);
        when(commandPrompt.getIntInput(anyString())).thenReturn(5);

        mainController.handleAddProductCommand();

        verify(inventory, times(1)).addProduct(any());

        verify(commandPrompt).displayMessage("Product added successfully. Assigned ID: 1");
    }

    @Test
    void testHandleViewProductCommand() {
        when(commandPrompt.getIntInput(anyString())).thenReturn(1);

        Product mockProduct = new Product(1, "Product Name", "Category", 9.99, 5);
        when(inventory.read(1)).thenReturn(mockProduct);

        mainController.handleViewProductCommand();

        verify(productDetailsView).displayProductDetails(mockProduct);
        verify(productDetailsView, never()).displayErrorMessage(anyString());
    }


    @Test
    void testHandleDeleteProductCommand() {
        when(commandPrompt.getIntInput(anyString())).thenReturn(1);

        Product mockProduct = new Product(1, "Product Name", "Category", 9.99, 5);
        when(inventory.read(1)).thenReturn(mockProduct);

        mainController.handleDeleteProductCommand();

        verify(inventory).delete(mockProduct);
        verify(commandPrompt).displayMessage("Product deleted successfully.");
    }

    @Test
    void testHandleSearchProductCommand() {
        when(commandPrompt.getInput(anyString())).thenReturn("Product Name");
        when(commandPrompt.getDoubleInput(anyString())).thenReturn(0.0, 10.0);

        Product mockProduct = new Product(1, "Product Name", "Category", 9.99, 5);
        when(inventory.searchProducts(anyString(), anyDouble(), anyDouble())).thenReturn(Collections.singletonList(mockProduct));

        mainController.handleSearchCommand();

        verify(searchResultsView).displaySearchResults(Collections.singletonList(mockProduct), 0.0, 10.0);
        verify(searchResultsView, never()).displayNoProductsFound();
    }
}
