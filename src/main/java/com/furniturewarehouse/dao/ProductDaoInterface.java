package com.furniturewarehouse.dao;

import com.furniturewarehouse.entity.Product;

import java.util.List;

public interface ProductDaoInterface {
    void create(Product product);
    Product read(int id);
    void update(Product product);
    void delete(int id);
    List<Product> searchProducts(String searchTerm, double minPrice, double maxPrice);
    List<Product> getAllProducts();

}
