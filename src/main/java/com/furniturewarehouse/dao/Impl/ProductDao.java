package com.furniturewarehouse.dao.Impl;

import com.furniturewarehouse.entity.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductDao implements com.furniturewarehouse.dao.ProductDaoInterface {
    private final Map<Integer, Product> productMap;

    public ProductDao() {
        productMap = new HashMap<>();
    }

    @Override
    public void create(Product product) {
        productMap.put(product.getId(), product);
    }

    @Override
    public Product read(int id) {
        return productMap.get(id);
    }

    @Override
    public void update(Product product) {
        productMap.put(product.getId(), product);
    }

    @Override
    public void delete(int id) {
        productMap.remove(id);
    }

    @Override
    public List<Product> searchProducts(String searchTerm, double minPrice, double maxPrice) {
        return productMap.values().stream()
                .filter(product -> (product.getName().equalsIgnoreCase(searchTerm) ||
                        product.getCategory().equalsIgnoreCase(searchTerm))
                        && product.getPrice() >= minPrice && product.getPrice() <= maxPrice)
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> getAllProducts() {
        return new ArrayList<>(productMap.values());
    }

}
