package com.furniturewarehouse.controller;


import com.furniturewarehouse.entity.Product;
import com.furniturewarehouse.service.InventoryInterface;
import com.furniturewarehouse.view.CommandPrompt;
import com.furniturewarehouse.view.ProductDetails;
import com.furniturewarehouse.view.SearchResults;
import java.util.List;

public class MainController  {
    private final InventoryInterface inventory;
    private final CommandPrompt commandPrompt;
    private final ProductDetails productDetailsView;

    private final SearchResults resultsView;

    public MainController(InventoryInterface inventory, CommandPrompt commandPrompt,
                          SearchResults resultsView, ProductDetails productDetailsView) {
        this.resultsView = resultsView;
        this.inventory = inventory;
        this.commandPrompt = commandPrompt;
        this.productDetailsView = productDetailsView;

    }

    public void start() {
        displayApplicationInfo();

        boolean running = true;
        while (running) {
            commandPrompt.displayMenu();
            String command = commandPrompt.getInput("Enter a command:");

            switch (command) {
                case "add" -> handleAddProductCommand();
                case "view" -> handleViewProductCommand();
                case "update" -> handleUpdateProductCommand();
                case "delete" -> handleDeleteProductCommand();
                case "search" -> handleSearchCommand();
                case "list" -> handleListAllProductsCommand();
                case "exit" -> running = false;
                default -> commandPrompt.displayErrorMessage("Invalid command. Please try again.");
            }
        }
    }

    private void displayApplicationInfo() {
        commandPrompt.displayMessage("Furniture Warehouse System");
        commandPrompt.displayMessage("Version: 5.0");
        commandPrompt.displayMessage("Created by: Izabella");
        commandPrompt.displayMessage("Email: some@gmail.com");
        commandPrompt.displaySeparator();
    }

    public void handleAddProductCommand() {
        int newProductId = inventory.getLastProductId() + 1;

        String productName = commandPrompt.getInput("Enter the product name:");
        String productCategory = commandPrompt.getInput("Enter the product category:");
        double productPrice = commandPrompt.getDoubleInput("Enter the product price:");
        int productQuantity = commandPrompt.getIntInput("Enter the product quantity:");

        Product newProduct = new Product(newProductId, productName, productCategory, productPrice, productQuantity);

        inventory.addProduct(newProduct);

        commandPrompt.displayMessage("Product added successfully. Assigned ID: " + newProductId);
    }

    public void handleViewProductCommand() {
        int productId = commandPrompt.getIntInput("Enter the product ID:");
        Product product = inventory.read(productId);
        if (product == null) {
            commandPrompt.displayMessage("Product not found.");
        } else {
            productDetailsView.displayProductDetails(product);
        }
    }

    private void handleUpdateProductCommand() {
        int productId = commandPrompt.getIntInput("Enter the product ID:");
        Product product = inventory.read(productId);
        if (product == null) {
            commandPrompt.displayMessage("Product not found.");
        } else {
            productDetailsView.displayProductDetails(product);

            String updatedName = commandPrompt.getInput("Enter the updated product name:");
            String updatedCategory = commandPrompt.getInput("Enter the updated product category:");
            double updatedPrice = commandPrompt.getDoubleInput("Enter the updated product price:");
            int updatedQuantity = commandPrompt.getIntInput("Enter the updated product quantity:");

            product.setName(updatedName);
            product.setCategory(updatedCategory);
            product.setPrice(updatedPrice);
            product.setQuantity(updatedQuantity);

            inventory.updateProduct(product);

            commandPrompt.displayMessage("Product updated successfully.");
        }
    }

    public void handleDeleteProductCommand() {
        int productId = commandPrompt.getIntInput("Enter the product ID:");
        Product product = inventory.read(productId);
        if (product == null) {
            commandPrompt.displayMessage("Product not found.");
        } else {
            inventory.delete(product);
            commandPrompt.displayMessage("Product deleted successfully.");
        }
    }

    public void handleSearchCommand() {
        String searchParameter = commandPrompt.getInput("Enter search parameter (name or category):");
        double minPrice = commandPrompt.getDoubleInput("Enter minimum price:");
        double maxPrice = commandPrompt.getDoubleInput("Enter maximum price:");

        try {
            if (minPrice > maxPrice) {
                resultsView.displayErrorMessage("Minimum price cannot be greater than maximum price.");
                return;
            }
            List<Product> searchResults = inventory.searchProducts(searchParameter, minPrice, maxPrice);
            if (searchResults.isEmpty()) {
                resultsView.displayNoProductsFound();
            } else {
                resultsView.displaySearchResults(searchResults, minPrice, maxPrice);
            }
        } catch (Exception e) {

            resultsView.displayErrorMessage("An error occurred while processing the search command.");
        }
    }


    private void handleListAllProductsCommand() {
        List<Product> allProducts = inventory.getAllProducts();
        if (allProducts.isEmpty()) {
            commandPrompt.displayMessage("No products found in the inventory.");
        } else {
            for (Product product : allProducts) {
                productDetailsView.displayProductDetails(product);
            }
        }
    }
}
