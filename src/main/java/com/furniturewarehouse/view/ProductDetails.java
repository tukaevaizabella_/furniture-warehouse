package com.furniturewarehouse.view;

import com.furniturewarehouse.entity.Product;

public class ProductDetails  {

    public void displayProductDetails(Product product) {
        System.out.println("Product Details:");
        System.out.println("ID: " + product.getId());
        System.out.println("Name: " + product.getName());
        System.out.println("Category: " + product.getCategory());
        System.out.println("Price: $" + product.getPrice());
        System.out.println("Quantity: " + product.getQuantity());
        System.out.println("----------------------------------");
    }

    public void displayErrorMessage(String s) {
        System.out.println("Error: " + s);
    }
}
