package com.furniturewarehouse.view;

import java.util.Scanner;

public class CommandPrompt {
    private final Scanner scanner;

    public CommandPrompt() {
        scanner = new Scanner(System.in);
    }


    public void displayMenu() {
        displayMessage("Available commands:");
        displayMessage(" - add: Add a product");
        displayMessage(" - view: View product details");
        displayMessage(" - update: Update a product");
        displayMessage(" - delete: Delete a product");
        displayMessage(" - search: Search for products");
        displayMessage(" - list: List all products");
        displayMessage(" - exit: Exit the application");
        displaySeparator();
    }


    public String getInput(String message) {
        displayMessage(message);
        displayMessage("> ");

        String input = scanner.nextLine().trim();
        while (input.isEmpty()) {
            input = scanner.nextLine().trim();
        }

        return input;
    }


    public int getIntInput(String message) {
        while (true) {
            try {
                displayMessage(message);
                displayMessage("> ");
                return Integer.parseInt(scanner.nextLine().trim());
            } catch (NumberFormatException e) {
                displayErrorMessage("Invalid input. Please enter a valid integer value.");
            }
        }
    }


    public double getDoubleInput(String message) {
        while (true) {
            try {
                displayMessage(message);
                displayMessage("> ");
                return Double.parseDouble(scanner.nextLine().trim());
            } catch (NumberFormatException e) {
                displayErrorMessage("Invalid input. Please enter a valid numeric value.");
            }
        }
    }

    public void displayMessage(String message) {
        System.out.println(message);
    }

    public void displayErrorMessage(String errorMessage) {
        displayMessage("Error: " + errorMessage);
    }


    public void displaySeparator() {
        displayMessage("----------------------------------------");
    }
}
