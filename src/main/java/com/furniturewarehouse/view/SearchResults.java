package com.furniturewarehouse.view;

import com.furniturewarehouse.entity.Product;

import java.util.List;

public class SearchResults{

    public void displaySearchResults(List<Product> products, double minPrice, double maxPrice) {
        if (products.isEmpty()) {
            displayNoProductsFound();
        } else {
            System.out.println("Search Results:");
            System.out.printf("%-5s | %-20s | %-10s | %-10s | %-8s%n", "ID", "Name", "Category", "Price", "Quantity");
            System.out.println("--------------------------------------------------------------");
            for (Product product : products) {
                displayProductDetails(product);
            }
            System.out.println("Price Range: $" + minPrice + " - $" + maxPrice);
        }
    }


    public void displayProductDetails(Product product) {
        System.out.printf("%-5s | %-20s | %-10s | %-10.2f | %-8d%n", product.getId(), product.getName(),
                product.getCategory(), product.getPrice(), product.getQuantity());
    }


    public void displayNoProductsFound() {
        System.out.println("No products found.");
    }


    public void displayErrorMessage(String errorMessage) {
        System.out.println("Error: " + errorMessage);
    }
}
