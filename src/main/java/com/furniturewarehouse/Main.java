package com.furniturewarehouse;

import com.furniturewarehouse.controller.MainController;

import com.furniturewarehouse.dao.Impl.ProductDao;
import com.furniturewarehouse.dao.ProductDaoInterface;

import com.furniturewarehouse.service.Impl.Inventory;
import com.furniturewarehouse.service.InventoryInterface;
import com.furniturewarehouse.view.CommandPrompt;
import com.furniturewarehouse.view.SearchResults;
import com.furniturewarehouse.view.ProductDetails;

public class Main {
    public static void main(String[] args) {
        ProductDaoInterface ProductDaoInterface = new ProductDao();
        InventoryInterface inventory = new Inventory(ProductDaoInterface);
        CommandPrompt commandPrompt = new CommandPrompt();
        SearchResults searchResults = new SearchResults();
        ProductDetails productDetails = new ProductDetails();

        MainController controller = new MainController(
                inventory, commandPrompt, searchResults, productDetails);


        controller.start();
    }
}
