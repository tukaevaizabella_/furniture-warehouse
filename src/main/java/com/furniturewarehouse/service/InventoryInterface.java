package com.furniturewarehouse.service;

import com.furniturewarehouse.entity.Product;

import java.util.List;

public interface InventoryInterface {
    void loadInventory();

    void addProduct(Product product);

    List<Product> searchProducts(String searchTerm, double minPrice, double maxPrice);

    List<Product> getAllProducts();

    Product read(int id);

    void updateProduct(Product product);

    int getLastProductId();

    void delete(Product product);

    void updateInventoryFile();
}
