package com.furniturewarehouse.service.Impl;

import com.furniturewarehouse.dao.ProductDaoInterface;
import com.furniturewarehouse.entity.Product;

import java.io.*;
import java.util.List;
import java.util.Scanner;

public class Inventory implements com.furniturewarehouse.service.InventoryInterface {
    private final ProductDaoInterface productDao;
    String inventoryFilePath = "src/main/resources/inventory.txt";
    public Inventory(ProductDaoInterface productDao) {
        this.productDao = productDao;
        loadInventory();
    }

    @Override
    public void loadInventory() {
        File file = new File(inventoryFilePath);
        if (!file.exists()) {
            System.out.println("Inventory file not found.");
            return;
        }

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] data = line.split(",");

                if (data.length == 5) {
                    int id;
                    String name = data[1];
                    String category = data[2];
                    double price;
                    int quantity;

                    try {
                        id = Integer.parseInt(data[0]);
                        price = Double.parseDouble(data[3]);
                        quantity = Integer.parseInt(data[4]);
                    } catch (NumberFormatException e) {
                        // Handle invalid numeric values in the file
                        System.out.println("Invalid data found in inventory file. Skipping line: " + line);
                        continue;
                    }

                    Product product = new Product(id, name, category, price, quantity);
                    productDao.create(product); // Adds the product to the data access layer
                } else {
                    System.out.println("Invalid data format found in inventory file. Skipping line: " + line);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Failed to load inventory: File not found.");
        }
    }

    @Override
    public void addProduct(Product product) {
        int nextProductId = getLastProductId() + 1;
        product.setId(nextProductId);
        productDao.create(product);
        updateInventoryFile();
    }

    @Override
    public List<Product> searchProducts(String searchTerm, double minPrice, double maxPrice) {
        return productDao.searchProducts(searchTerm, minPrice, maxPrice);
    }

    @Override
    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }

    @Override
    public Product read(int id) {
        return productDao.read(id);
    }

    @Override
    public void updateProduct(Product product) {
        productDao.update(product);
        updateInventoryFile();
    }

    @Override
    public int getLastProductId() {
        List<Product> allProducts = productDao.getAllProducts();
        int lastProductId = 0;
        for (Product product : allProducts) {
            if (product.getId() > lastProductId) {
                lastProductId = product.getId();
            }
        }
        return lastProductId;
    }

    public void updateInventoryFile() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(inventoryFilePath))) {
            List<Product> allProducts = productDao.getAllProducts();
            for (Product product : allProducts) {
                writer.println(product.getId() + "," + product.getName() + "," +
                        product.getCategory() + "," + product.getPrice() + "," + product.getQuantity());
            }
        } catch (IOException e) {
            System.out.println("Failed to update inventory file: " + e.getMessage());
        }
    }

    @Override
    public void delete(Product product) {
        productDao.delete(product.getId());
        updateInventoryFile();
    }
}
